package com.example.exer04;

public class Voiture {

    private String model;
    private String color;
    private String price;
    private int image;

    public Voiture (String model, String color, String price, int image){
        this.model = model;
        this.color = color;
        this.price = price;
        this.image = image;
    }

    public String getModel(){
        return model;
    }

    public String getColor(){
        return color;
    }

    public String getPrice(){
        return price;
    }

    public int getImage() {
        return image;
    }
}
