package com.example.exer03;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.app.*;
import android.app.Activity;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.*;
import android.widget.*;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_liste);
        ArrayList<Cours> listeCours = new ArrayList<Cours>();
        initialiserListe(listeCours);
        AdapterListeCours adapter = new AdapterListeCours(this, R.layout.item_liste, listeCours);
        ListView listView = (ListView) findViewById(R.id.idListeCours);
        listView.setAdapter(adapter);
    }

    private void initialiserListe(ArrayList<Cours> listeCours){
        listeCours.add(new Cours("C/C++", R.drawable.animal1));
        listeCours.add(new Cours("Java", R.drawable.animal2));
        listeCours.add(new Cours("HTML", R.drawable.animal3));
        listeCours.add(new Cours("CSS", R.drawable.animal4));
        listeCours.add(new Cours("JAvaScript", R.drawable.animal2));
        listeCours.add(new Cours("Visual Basic", R.drawable.animal1));
        listeCours.add(new Cours("PHP", R.drawable.animal4));
        listeCours.add(new Cours("MySQL", R.drawable.animal3));
    }
}