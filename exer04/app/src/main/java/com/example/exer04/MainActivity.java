package com.example.exer04;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_liste);
        ArrayList<Voiture> listeVoiture = new ArrayList<Voiture>();
        initialiserListe(listeVoiture);
        AdapterListeVoiture adapter = new AdapterListeVoiture(this, R.layout.item_liste, listeVoiture);
        ListView listView = (ListView) findViewById(R.id.idListeVoiture);
        listView.setAdapter(adapter);
    }

    private void initialiserListe (ArrayList<Voiture> listeVoiture){
        listeVoiture.add(new Voiture ("Mercedes Class E 450", "Designo White", "80 000.00$", R.drawable.mercoe));
        listeVoiture.add(new Voiture ("Mercedes Class E 63s", "Black", "180 000.00$", R.drawable.mercoe2));
        listeVoiture.add(new Voiture ("Mercedes AMG GT 63s", "Red", "195 000.00$", R.drawable.mercoe3));
        listeVoiture.add(new Voiture ("Mercedes Class G 63s", "Designo Black", "280 000.00$", R.drawable.mercoe4));
        listeVoiture.add(new Voiture ("Mercedes Class S 580", "Design Red", "188 000.00$", R.drawable.mercoe5));
        listeVoiture.add(new Voiture ("Mercedes AMG SL 63s", "Designo Blue", "104 000.00$", R.drawable.mercoe6));
        listeVoiture.add(new Voiture ("Mercedes Class C 63s", "Dark Gray", "90 000.00$", R.drawable.mercoe7));
        listeVoiture.add(new Voiture ("Mercedes EQS AMG 53", "Senite Gray", "160 000.00$", R.drawable.mercoe8));
    }
}