package com.example.exer04;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.exer04.R;
import com.example.exer04.Voiture;

import java.util.ArrayList;

public class AdapterListeVoiture extends ArrayAdapter<Voiture> {

    private Context context;
    private int layoutItemListe;
    private Resources res;
    ArrayList<Voiture> listeVoiture;

    public AdapterListeVoiture(Context context, int layoutItemListe, ArrayList<Voiture> listeVoiture){
        super(context, layoutItemListe, listeVoiture);
        this.context = context;
        this.layoutItemListe = layoutItemListe;
        res = context.getResources();
        this.listeVoiture = listeVoiture;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view = convertView;
        if(view == null){
            LayoutInflater layoutInflater = (LayoutInflater)  context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE
            );
            view = layoutInflater.inflate(layoutItemListe, parent, false);
            Voiture voiture = listeVoiture.get(position);
            if (voiture !=null){
                TextView textViewModelVoiture = (TextView) view.findViewById(R.id.model);
                TextView textViewColorVoiture = (TextView) view.findViewById(R.id.color);
                TextView textViewPriceVoiture = (TextView) view.findViewById(R.id.price);
                ImageView imageViewVoiture = (ImageView) view.findViewById(R.id.icon);

                textViewModelVoiture.setText(voiture.getModel());
                textViewColorVoiture.setText(voiture.getColor());
                textViewPriceVoiture.setText(voiture.getPrice());
                imageViewVoiture.setImageResource(voiture.getImage());

            }
        }
        return view;
    }

    @Override
    public int getCount(){
        return listeVoiture.size();
    }

}
