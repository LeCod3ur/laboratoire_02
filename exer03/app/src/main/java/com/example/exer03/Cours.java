package com.example.exer03;

public class Cours {
    private String nom;
    private int image;

    public Cours (String nom, int image) {
        this.nom = nom;
        this.image = image;
    }

    public String getNom(){
        return nom;
    }

    public int getImage(){
        return image;
    }
}
